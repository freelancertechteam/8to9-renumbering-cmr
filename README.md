![freelancertech.png](https://bitbucket.org/repo/xGKzre/images/358750387-freelancertech.png)
### Description ###
Le code source de l'application android 8to9 Renumbering CMR permet de passer vos numéros du Cameroun de 8 à 9 chiffre et constitue un bon départ pour les développeurs qui souhaitent se lancer dans le développement des applications mobiles sur système Android.

**Téléchargeable sur Google Play**
[https://play.google.com/store/apps/details?id=com.frl.android.safecontacts ](https://play.google.com/store/apps/details?id=com.frl.android.safecontacts )

### Captures ###
![safecontact1.jpg](https://bitbucket.org/repo/xGKzre/images/137457180-safecontact1.jpg)
![safecontact2.jpg](https://bitbucket.org/repo/xGKzre/images/2585364310-safecontact2.jpg)
![unnamed.png](https://bitbucket.org/repo/xGKzre/images/1035048307-unnamed.png)
![unnamed4.png](https://bitbucket.org/repo/xGKzre/images/2569937193-unnamed4.png)
![unnamed2.png](https://bitbucket.org/repo/xGKzre/images/4017563319-unnamed2.png)
![unnamed3.png](https://bitbucket.org/repo/xGKzre/images/1427931905-unnamed3.png)