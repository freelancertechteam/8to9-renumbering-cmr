/**
 * SafeContacts
 * Copyright (C)2014 FREELANCERTECH
	Contact: support@freelancertech.net
	Author:  Raph FONGANG 
	Contributor(s): Daniel Fouomene    
 */
package net.freelancertech.safecontacts;

import net.freelancertech.safecontacts.utils.SafeConstants;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;



public class ResultTransformContactsActivity extends Activity{
	int mobile=0;
	int fixe=0;
	int notChange=0;
	static String TAG="ResultTransformContactsActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result_contats_transformation_view);
		
		TextView txtMobile = (TextView)this.findViewById(R.id.textViewCountMobile);
		TextView txtFixe = (TextView)this.findViewById(R.id.textViewCountFixe);
		TextView txtNotChange = (TextView)this.findViewById(R.id.textViewCountNotChange);
		


			// TODO:
			// Restore value of counters from saved state
			// Only need 4 lines of code, one for every count variable
			mobile = getIntent().getExtras().getInt(SafeConstants.COUNT_MOBILE);
			fixe = getIntent().getExtras().getInt(SafeConstants.COUNT_FIXE);
			notChange = getIntent().getExtras().getInt(SafeConstants.COUNT_NOT_CHANGE);		

		txtMobile.setText(getString(R.string.resultMsgMobile, mobile+""));
		txtFixe.setText(getString(R.string.resultMsgFixe, fixe+""));
		txtNotChange.setText(getString(R.string.resultMsgAutres, notChange+""));						
	}
	
	@Override
    protected void onDestroy() {
		
        super.onDestroy();
        
    }
	
	public void backToHome(View v){
		Intent intent = new Intent(ResultTransformContactsActivity.this,SafeContactsApplication.class);
		startActivity(intent);
	}
}
