/**
 * SafeContacts
 * Copyright (C)2014 FREELANCERTECH
	Contact: support@freelancertech.net
	Author:  Raph FONGANG 
	Contributor(s): Daniel Fouomene    
 */
package net.freelancertech.safecontacts;

import java.util.ArrayList;
import java.util.List;

import net.freelancertech.safecontacts.adapters.ContactsListAdapter;
import net.freelancertech.safecontacts.beans.Contacts;
import net.freelancertech.safecontacts.beans.PhoneNumber;
import net.freelancertech.safecontacts.utils.SafeConstants;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;



public class ContactsListActivity extends ListActivity{
	
	 ContactsListAdapter dataAdapter = null;
	 static String  TAG="ContactsListActivity";
	 
	ProgressDialog progressBar;
	private int progressBarStatus = 0;
	private Handler progressBarHandler = new Handler();
	ListView listView=null;
	
	int countMobile=0;
	int countFixe=0;
	int countNotChange=0;
	List<Contacts> contactList=null;
	int transform=1;
	
	String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
	Uri uriContacts = ContactsContract.Contacts.CONTENT_URI;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.contacts_list_view);
	 
	  //Generate list View from ArrayList
	  this.getListContacts();	
	  	 
	 }
	 
	 private  void  getListContacts() {
			// prepare for a progress bar dialog
			progressBar = new ProgressDialog(this);
			progressBar.setCancelable(true);
			progressBar.setTitle(R.string.progressBarTitleDisplayContact);		
			progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressBar.setProgress(0);

			contactList= new ArrayList<Contacts>();
			
			ContentResolver cr = getContentResolver();
			Cursor cur = cr.query(uriContacts, null,null, null, sortOrder);

			progressBar.setMax(cur.getCount());
			progressBar.show();
			
			//reset progress bar status
			progressBarStatus = 0;
			Thread t = new Thread(new Runnable() {
				  public void run() {	
				ContentResolver cr = getContentResolver();
				Cursor cur = cr.query(uriContacts, null,null, null, sortOrder);
		
				 
		
				 //parcours de tous les contacts
				 int count=0;
				 int countSize=cur.getCount();
				if (cur.getCount() > 0) {
				    while (cur.moveToNext()) {
				    	count++;
				        String id = cur.getString(
			                        cur.getColumnIndex(ContactsContract.Contacts._ID));
					String name = cur.getString(
			                        cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
					
					Contacts contact = new Contacts();
					contact.setId(id);
					contact.setName(name);
					
					// Obtentino des Num�ros de t�l�phone
			 		if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
			 			Cursor pCur = cr.query(
			 		 		    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
			 		 		    null, 
			 		 		    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", 
			 		 		    new String[]{id}, null);
			 		 	        while (pCur.moveToNext()) {
			 		 				String idTel = pCur.getString(
			 		 						pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));	 		 	        	
			 		 				String numberTel = pCur.getString(
			 		 						pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			 		 				contact.getTelephones().add(new PhoneNumber(idTel, numberTel));
			 		 	        } 
			 		 	        pCur.close(); 
			 	      }
			 		
			 		//Obtention des Emails
			 		/*Cursor emailCur = cr.query( 
			 				ContactsContract.CommonDataKinds.Email.CONTENT_URI, 
			 				null,
			 				ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", 
			 				new String[]{id}, null); 
			 			while (emailCur.moveToNext()) { 
			 			    String email = emailCur.getString(
			 		                      emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
			 		 	  contact.getEmails().add(email);
			 		 	} 
			 		 	emailCur.close();
			 		*/
			 		// ajout � la liste
			 		  contactList.add(contact);
			 		 progressBarStatus =(count>=countSize)?(100):(count*100/countSize);
			 		 
						 // Update the progress bar
						 progressBarHandler.post(new Runnable() {
							 public void run() {
								  progressBar.setProgress(progressBarStatus);
								  if (progressBarStatus >= 100) {
									  if(contactList!=null && contactList.size()>0){
										  dataAdapter = new ContactsListAdapter(ContactsListActivity.this,
												    R.layout.contacts_list_item, contactList);
										  	  		  
										  listView = getListView();
										  /*View headerView = View.inflate(this, R.layout.contacts_list_view_header, null);
										  listView.addHeaderView(headerView);
										  View footerView = View.inflate(this, R.layout.contacts_list_view_footer, null);
										  listView.addFooterView(footerView);	*/
										  
										  listView.setAdapter(dataAdapter);
									  }else{
										  Toast.makeText(getApplication(), R.string.noneContacts, Toast.LENGTH_LONG).show();
										  Intent intent = new Intent(ContactsListActivity.this,SafeContactsApplication.class);
										  startActivity(intent);
									  }										  
								  }
							  }
						 });	 		  
			        }
			 	}		 
				// ok, file is downloaded,
				if (progressBarStatus >= 100) {

					// sleep 2 seconds, so that you can see the 100%
					try {
						Thread.sleep(2000);
						// close the progress bar dialog
						progressBar.dismiss();	
						
						
						//Toast.makeText(ContactsListActivity.this, "Updated Done", Toast.LENGTH_SHORT).show();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			  }
		});
		
		t.start();
		
		  
		  
			
		//return contactList;
		 
		  //create an ArrayAdaptar from the String Array

		//  ListView listView = (ListView) findViewById(R.id.contact_list);
		  // Assign adapter to ListView
		  //listView.setAdapter(dataAdapter);	 	 
		 
		 }
	 
	 @Override
	  protected void onListItemClick(ListView l, View v, int position, long id)
	  {
		    // When clicked, show a toast with the TextView text
		    Contacts contact = (Contacts) l.getItemAtPosition(position);
		    Toast.makeText(getApplicationContext(),
		      "Clicked on Row: " + contact.getName(), 
		      Toast.LENGTH_LONG).show();
	   }
	 
	 public void checkAllContacts(View v){
		 ListView listView = getListView();
		 CheckBox cbHeader = (CheckBox)this.findViewById(R.id.checkbox_all);
		 Log.i(TAG, " listView.getCount()"+listView.getChildCount());
		 for(int i=0; i < listView.getChildCount(); i++){
			    RelativeLayout itemLayout = (RelativeLayout)listView.getChildAt(i);
			    CheckBox cb;
			    
			    for(int j=0; j < itemLayout.getChildCount();j++){
			    	Log.i(TAG, " debut for j = "+j);			    	
			    	Log.i(TAG, " if debut for itemLayout.getChildAt(j).getId() = "+itemLayout.getChildAt(j).getId());
			    	Log.i(TAG, " debut for R.id.checkBox1 = "+R.id.checkBox1);
			    	if(itemLayout.getChildAt(j).getId() == R.id.checkBox1){
			    		cb = (CheckBox)itemLayout.getChildAt(j);
			    		cb.setChecked(cbHeader.isChecked());
			    		break;
			    	}
			    }			    			    
			}
	 }
	 
	 public void onScroll(AbsListView v, int firstVisibleItem, int visibleCount, int totalItemCount)
	 {
		 ListView listView = getListView();
		 CheckBox cbHeader = (CheckBox)this.findViewById(R.id.checkbox_all);

	     for (int i = 0; i < listView.getChildCount(); i++)
	     {
			    RelativeLayout itemLayout = (RelativeLayout)listView.getChildAt(i);
			    CheckBox cb;	    	 
			    for(int j=0; j < itemLayout.getChildCount();j++){
			    	Log.i(TAG, " debut for j = "+j);			    	
			    	Log.i(TAG, " if debut for itemLayout.getChildAt(j).getId() = "+itemLayout.getChildAt(j).getId());
			    	Log.i(TAG, " debut for R.id.checkBox1 = "+R.id.checkBox1);
			    	if(itemLayout.getChildAt(j).getId() == R.id.checkBox1){
			    		cb = (CheckBox)itemLayout.getChildAt(j);
			    		cb.setChecked(cbHeader.isChecked());
			    		break;
			    	}
			    }
	     }
	 }	 
	 
	 /**
	  * Transforme un chiffre en respectant les nouvelles normes de l'ART
	  * 
	  * @param v
	  */
	 public void transformNumberExecute(View v){
		 

		// prepare for a progress bar dialog
		progressBar = new ProgressDialog(v.getContext());
		progressBar.setCancelable(true);
		progressBar.setMessage(getString(R.string.progressBarMessagePreparing));		
		progressBar.setTitle(R.string.progressBarTitle);		
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setProgress(0);

		 ListView listView = getListView();
		 int listCount =listView.getCount();
			progressBar.setMax(listCount);
			progressBar.show();		
		//reset progress bar status
		progressBarStatus = 0;
		
		new Thread(new Runnable() {
			  public void run() {
					
				  // process some tasks
				  //progressBarStatus = doSomeTasks();
				  
				 //begin tranformation
				 ListView listView = getListView();
				 int listCount =listView.getCount();
				 int totalNumber=0;
				 for(int i=0; i < listCount; i++){
					 Contacts contact = (Contacts) listView.getItemAtPosition(i);
					 if(contact!=null && contact.isSelected()){
						// Log.i(TAG, " contact "+contact.getName());
						 //Log.i(TAG, " contact.getTelephones() "+contact.getTelephones());
						 List<PhoneNumber> listTelephones = contact.getTelephones();
						 //progressBar.setMessage(getString(R.string.progressBarMessage,contact.getName()));
						 if(listTelephones!=null){
							 for (PhoneNumber phone : listTelephones) {
									 String convertPhone = ajustPhoneForOperator( PhoneNumberUtils.stripSeparators(phone.getNumber()));
									 totalNumber++;
									 Log.i(TAG, " contact Avant: "+PhoneNumberUtils.stripSeparators(phone.getNumber())+" | Apr�s: "+convertPhone);
									 updateAllContact(contact.getName(),PhoneNumberUtils.formatNumber(convertPhone)); //ajustPhoneForOperator(phone.getNumber())
							 }
						 }	
						 progressBarStatus = (i+1);//(100*(i+1))/listCount;
					 }
					 // your computer is too fast, sleep 1 second
					 try {
						Thread.sleep(1000);
					 } catch (InterruptedException e) {
						e.printStackTrace();
					 }	
					  
					 
					 if((i+1)==listCount){
						 progressBarStatus=100;
					 }
					
					 //dataAdapter.notifyDataSetChanged();
					 
					 // Update the progress bar
					 progressBarHandler.post(new Runnable() {
						 public void run() {
							  progressBar.setProgress(progressBarStatus);
						  }
					 });					  
				 }
					 
				

				// ok, file is downloaded,
				if (progressBarStatus >= 100) {

					// sleep 2 seconds, so that you can see the 100%
					try {
						Thread.sleep(2000);
						// close the progress bar dialog
						progressBar.dismiss();	
						
						  Intent intentResut = new Intent(ContactsListActivity.this,ResultTransformContactsActivity.class);
						  intentResut.putExtra(SafeConstants.COUNT_FIXE, countFixe);
						  intentResut.putExtra(SafeConstants.COUNT_MOBILE, countMobile);
						  
						  countNotChange = totalNumber -countFixe-countMobile;
						  intentResut.putExtra(SafeConstants.COUNT_NOT_CHANGE,countNotChange);	
						  Log.i(TAG,"countFixe ="+countFixe);
						  Log.i(TAG,"countMobile ="+countMobile);
						  Log.i(TAG,"countNotChange ="+countNotChange);
						  startActivity(intentResut);						
						//Toast.makeText(ContactsListActivity.this, "Updated Done", Toast.LENGTH_SHORT).show();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			  }
		}).start();		
			
		//refresh list
		//
		  //Generate list View from ArrayList
		 /* ArrayList<Contacts> contactList = (ArrayList<Contacts>)this.getListContacts();	
		  
		  dataAdapter = new ContactsListAdapter(this,
				    R.layout.contacts_list_item, contactList);	
		  //listView.invalidate();
		  dataAdapter.notifyDataSetChanged();							
		  listView.invalidate();*/
		

		
	 }
	 
	 
	 private void updateAllContact(String name, String phone) {
	                	        
	        
			ContentResolver cr = getContentResolver();
			ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
			
			
			String where =
					ContactsContract.Data.DISPLAY_NAME + " = ? AND " +
                    ContactsContract.Data.MIMETYPE + " = ? ";
			String[] params = new String[] {name,
	                  ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE};
			
			ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(where, params)
                    .withValue(ContactsContract.CommonDataKinds.Phone.DATA, phone)
                    .build());
			//Log.i(TAG, " params = "+params[0]+" - "+params[1]+" - "+params[2]);
			try {
				cr.applyBatch(ContactsContract.AUTHORITY, ops);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OperationApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
	    }
	  
	 /**
	  * Fonction de transformation proprement dite
	  * @param phone
	  * @return
	  */
	 private String ajustPhoneForOperator(String phone){
		 String res=phone;
		 String[] prefix= new String[]{"4","5","6","7","8","9"};
		 String[] prefixFixe= new String[]{"2","3"};
		 
		 if(phone==null)
			 return res;
		 
		 //num�ro sans pr�fix du pays		 
		 if(phone.length()==8){	
			 //num�ro mobile
			 for (int i = 0; i < prefix.length; i++) {
				 if(phone.substring(0, 1).equals(prefix[i])){
					 res="6"+phone;
					 countMobile++;
					 break;
				}
			}
		 
			 //num�ro fixe camtel
			 if(phone.substring(0, 2).equals("22")){
				 res="242"+phone.substring(2);
				 countFixe++;
			}
			 else if(phone.substring(0, 2).equals("33")){
				 res="243"+phone.substring(2);
				 countFixe++;
			}
			 else{
				 for (int i = 0; i < prefixFixe.length; i++) {
					 if(phone.substring(0, 1).equals(prefixFixe[i])){
						 res="2"+phone;
						 countFixe++;
						 break;
					}
				}
			 }
				 
				
		 }
		 else  // num�ro avec +237
			 if(phone.length()==12){
				//num�ro mobile
				 for (int i = 0; i < prefix.length; i++) {
					 if(phone.substring(0, 5).equals("+237"+prefix[i])){
						 res="+2376"+phone.substring(4);
						 countMobile++;
						 break;
					}
				}	
				//num�ro fixe camtel
				 if(phone.substring(0, 6).equals("+23722")){
					 res="+237242"+phone.substring(6);
					 countFixe++;
				}
				 else if(phone.substring(0, 6).equals("+23733")){
					 res="+237243"+phone.substring(6);
					 countFixe++;
				}				 
				 else{
					 for (int i = 0; i < prefixFixe.length; i++) {
						 if(phone.substring(0, 5).equals("+237"+prefixFixe[i])){
							 res="+2372"+phone.substring(4);
							 countFixe++;
							 break;
					}
				 }
			}		 
		}			 			 
		 
		 else  // num�ro avec 00237
			 if(phone.length()==13){
				//num�ro mobile
				 for (int i = 0; i < prefix.length; i++) {
					 if(phone.substring(0, 6).equals("00237"+prefix[i])){
						 res="002376"+phone.substring(5);
						 countMobile++;
						 break;
					}
				}	
				//num�ro fixe camtel
				 if(phone.substring(0, 7).equals("0023722")){
					 res="00237242"+phone.substring(7);
					 countFixe++;
				}
				 else if(phone.substring(0, 7).equals("0023733")){
					 res="00237243"+phone.substring(7);
					 countFixe++;
				}	
				 else{
					 
					 for (int i = 0; i < prefixFixe.length; i++) {
						 if(phone.substring(0, 6).equals("00237"+prefixFixe[i])){
							 res="002372"+phone.substring(5);
							 countFixe++;
							 break;
						}
					}
				 }
					 
			 }			 			 
		 
		 return res;
	 }	



	 
/*	 private String ajustPhoneForOperator(String phone){
		 String res="";
		 String[] prefix= new String[]{"4","5","6","7","8","9"};
		 
		 //num�ro sans pr�fix du pays
		 if(phone.length()==8){	
			 //num�ro mobile
			 for (int i = 0; i < prefix.length; i++) {
				 if(phone.substring(0, 1).equals(prefix[i])){
					 res="6"+phone;
					 break;
				}
			}
		 
			 //num�ro fixe camtel
			if(phone.substring(0, 2).equals("22"))
				res="242"+phone.substring(3);
			else if(phone.substring(0, 2).equals("33"))
				res="343"+phone.substring(3);
				
		 }
		 else  // num�ro avec +237
			 if(phone.length()==12){
				 for (int i = 0; i < prefix.length; i++) {
					 if(phone.substring(0, 5).equals("+237"+prefix[i])){
						 res="+2376"+phone;
						 break;
					}
				}	
				 //num�ro fixe camtel
					if(phone.substring(0, 6).equals("22"))
						res="+237242"+phone.substring(7);
					else if(phone.substring(0, 6).equals("33"))
						res="+237343"+phone.substring(7);				 
			 }			 			 
		 
		 else  // num�ro avec 00237
			 if(phone.length()==12){
				 for (int i = 0; i < prefix.length; i++) {
					 if(phone.substring(0, 6).equals("00237"+prefix[i])){
						 res="002376"+phone;
						 break;
					}
				}	
				 //num�ro fixe camtel
					if(phone.substring(0, 7).equals("22"))
						res="00237242"+phone.substring(8);
					else if(phone.substring(0, 7).equals("33"))
						res="00237343"+phone.substring(8);				 
			 }			 			 
		 
		 return res;
	 }*/
}
