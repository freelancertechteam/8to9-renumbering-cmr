/**
 * SafeContacts
 * Copyright (C)2014 FREELANCERTECH
	Contact: support@freelancertech.net
	Author:  Raph FONGANG 
	Contributor(s): Daniel Fouomene    
 */
package net.freelancertech.safecontacts.beans;

import java.util.ArrayList;
import java.util.List;

public class Contacts {

	private String id="";

	private String name="";
	private List<PhoneNumber> telephones=null;
	private List<String> emails=null;
	boolean selected = true;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<PhoneNumber> getTelephones() {
		if(telephones==null){
			telephones = new ArrayList<PhoneNumber>();
		}
		return telephones;
	}
	public void setTelephones(List<PhoneNumber> telephones) {
		this.telephones = telephones;
	}
	public List<String> getEmails() {
		if(emails==null){
			emails = new ArrayList<String>();
		}		
		return emails;
	}
	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTelephoneToString(){
		String res="";
		if(telephones!=null){
			for (PhoneNumber tel: telephones) {
				res=(res.equals(""))?(tel.getNumber()):(res+" ; "+tel.getNumber());			
			}
		}
		return res;
	}
		
}


