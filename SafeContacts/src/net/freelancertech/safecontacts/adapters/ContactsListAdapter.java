/**
 * SafeContacts
 * Copyright (C)2014 FREELANCERTECH
	Contact: support@freelancertech.net
	Author:  Raph FONGANG 
	Contributor(s): Daniel Fouomene    
 */
package net.freelancertech.safecontacts.adapters;

import java.util.ArrayList;
import java.util.List;

import net.freelancertech.safecontacts.beans.Contacts;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import net.freelancertech.safecontacts.R;

public class ContactsListAdapter extends ArrayAdapter<Contacts>{
	 private ArrayList<Contacts> contactList;
	 private Context _context;
	 
	  public ContactsListAdapter(Context context, int textViewResourceId, 
	    List<Contacts> contactList) {
	   super(context, textViewResourceId, contactList);
	   this._context = context;
	   this.contactList = new ArrayList<Contacts>();
	   this.contactList.addAll(contactList);
	  }
	 
	  private class ViewHolder {
	   TextView contactName;
	   TextView phoneNumber;
	   CheckBox checkbox;
	  }
	 
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	 
	   ViewHolder holder = null;
	   Log.i("ConvertView", String.valueOf(position));
	   View view=convertView;
	   Contacts contacts = contactList.get(position);
	   
	   if (convertView == null) {
	   LayoutInflater vi = (LayoutInflater)this._context.getSystemService(
	     Context.LAYOUT_INFLATER_SERVICE);
	   view = vi.inflate(R.layout.contacts_list_item, null);
	 
	   holder = new ViewHolder();
	   holder.contactName = (TextView) view.findViewById(R.id.contact_name);
	   holder.phoneNumber = (TextView) view.findViewById(R.id.contact_numbers);
	   holder.checkbox = (CheckBox) view.findViewById(R.id.checkBox1);
	   
	 
	    holder.checkbox.setOnClickListener( new View.OnClickListener() {  
	     public void onClick(View v) {  
	      CheckBox cb = (CheckBox) v ;  
	      Contacts contact = (Contacts) cb.getTag();  
	      Toast.makeText(getContext(),
	       "Clicked on Checkbox: " + cb.getText() +
	       " is " + cb.isChecked(), 
	       Toast.LENGTH_LONG).show();
	      contact.setSelected(cb.isChecked());
	     }  
	    });  
	    		   


		   holder.checkbox.setTag(contacts);	    
	   } 
	   else {
	    holder = (ViewHolder) view.getTag();
	   }
	 
	   holder.contactName.setText(contacts.getName());
	   holder.phoneNumber.setText(contacts.getTelephoneToString());
	   holder.checkbox.setChecked(contacts.isSelected());
	   
	   view.setTag(holder);
	   
	   return view;
	 
	  }
	  
	  
}
