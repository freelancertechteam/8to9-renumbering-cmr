/**
 * SafeContacts
 * Copyright (C)2014 FREELANCERTECH
	Contact: support@freelancertech.net
	Author:  Raph FONGANG 
	Contributor(s): Daniel Fouomene    
 */
package net.freelancertech.safecontacts;

import net.freelancertech.safecontacts.utils.SafeConstants;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class SafeContactsApplication extends Activity {

	static String  TAG="SafeContactsApplication";
	int tranform=1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.safe_contacts_application);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.safe_contacts_application, menu);
		return true;
	}

	public void transformNumber(View view) {
		Intent intent = new Intent(this, ContactsListActivity.class);
		startActivity(intent);
	 }	
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		// TODO:
		savedInstanceState.putInt(SafeConstants.TRANSFORM, tranform);
	}	
}
