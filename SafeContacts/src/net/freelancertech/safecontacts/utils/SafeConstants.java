/**
 * SafeContacts
 * Copyright (C)2014 FREELANCERTECH
	Contact: support@freelancertech.net
	Author:  Raph FONGANG 
	Contributor(s): Daniel Fouomene    
 */
package net.freelancertech.safecontacts.utils;

public class SafeConstants {
	public static final String COUNT_MOBILE = "countMobile";
	public static final String COUNT_FIXE = "countFixe";
	public static final String COUNT_NOT_CHANGE = "countNotChange";
	
	public static final String TRANSFORM = "tranform";
}
